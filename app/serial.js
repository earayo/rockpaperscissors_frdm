var SerialPort = require('serialport');
var sp = new SerialPort('/dev/ttyACM0', {
  "baudRate": 19200,
  "parser": SerialPort.parsers.byteLength(1)
}, function() {
  sp.write('0');
});
var express = require('express');
var app = express()
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server);
server.listen(3001);

var SerialComponent = {
  "options": {
    "Empate!!!00": "a",
    "Empate!!!11": "b",
    "Empate!!!22": "c",
    "Computer win01": "d",
    "I win10": "e",
    "I win02": "f",
    "Computer win20": "g",
    "Computer win12": "h",
    "I win21": "i"
  }
}

SerialComponent.write = function (data, callback) {
  console.log("Send =======> ", data);
  sp.write(String(data));
}

SerialComponent.initConfig = function () {
  sp.on('open', function(data) {
    console.log("Open!!!!!");
  });

  sp.on('data', function(data) {
    console.log("wich is the data?", data.toString());
  });

  sp.on('error', function(err) {
    console.log("An error happened ====> ", err);
  });
};
SerialComponent.initConfig();

//on a socket connection
io.sockets.on('connection', function (socket) {
  // if servo message received
  socket.on('options', function (data) {
    console.log("Me...");
    var key = data.winner.concat(data.me).concat(data.computer);
    SerialComponent.write(SerialComponent["options"][key]);
  });
});

module.exports = SerialComponent;
