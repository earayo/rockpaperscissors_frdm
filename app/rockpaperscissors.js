var ComputerSerial = require('../app/serial.js');
var io = require('socket.io-client');
var coreGame = {
}
coreGame.play = function(optionSel) {
  var result = {};
  var socket = io.connect('http://localhost:3001');
  var option = this.getOptions();
  result.computerOptionCode = option;
  result.computerOptionDesc = this.getDescriptionSel(option);
  result.myOptionSelCode = optionSel;
  result.myOptionSelDescription = this.getDescriptionSel(optionSel);
  result.victory = this.getWinner(optionSel, option);
  socket.emit('options',{"me": optionSel, "computer": option, "winner": result.victory});
  return result;
}

coreGame.getOptions = function () {
  return Math.floor(Math.random() * (3));
}

coreGame.getDescriptionSel = function (optionSel) {
  var description = "";
  switch (optionSel) {
    case 0:
      description = "Rock";
      break;
    case 1:
      description = "Paper";
      break;
    case 2:
      description = "Scissors";
      break;
    default:
      description = "";
    break;
  }
  return description;
}

coreGame.isWinnerPaper = function(option1, option2) {
  return option1 == 0 && option2 == 1;
}

coreGame.isWinnerRock = function(option1, option2) {
  return option1 == 2 && option2 == 0;
}

coreGame.isWinnerScissors = function(option1, option2) {
  return option1 == 1 && option2 == 2;
}

coreGame.getWinner = function(miOptionSel, computerSel) {
  if (miOptionSel == computerSel) {
    return "Empate!!!";
  } else if (this.isWinnerRock(miOptionSel, computerSel) ||
      this.isWinnerPaper(miOptionSel, computerSel) ||
      this.isWinnerScissors(miOptionSel, computerSel)) {
    return "Computer win";
  } else {
    return "I win";
  }
}

module.exports = coreGame;
