# Rock Paper Scissors

Sample project using Node JS, Express Framework, Socket-IO, serialPort and FRDM-K2050M.

## Hardware required

* FRDM-K2050M Board.
* 2 Servos 9gr.
* NodeJS and express framework Installed.
* LED's (Red, Blue and Green  Ideally).
* Resistors of 220 Ohms

## Install dependencies

```bash
$ npm Install
```

## How to run?

* Connect your board.
* Verifiy the listen serial port and set SERIAL_PORT enviroment variable. (i.e /dev/ttyACM0)
* Set BAUD_RATE variable with baudios to listen in serial port (i.e 19200)
* Run the following command:
```bash
$ SERIAL_PORT=/dev/ttyACM0 BAUD_RATE=19200 npm start
```
